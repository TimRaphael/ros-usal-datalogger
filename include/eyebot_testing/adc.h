/**
 * \file			adc.h
 * \brief     Header file for the ADC functions
 * \author    Remi KEAT
 */

#ifndef ADC_H_
#define ADC_H_
/*

ADCHandle OSInitADC(DeviceSemantics semantics);
int OSADCRelease(ADCHandle handle);
int OSGetADC(ADCHandle adchandle);
int ConvADCSampleToVoltage(ADCHandle adchandle, char *volt, int sample);
 */

#endif /* ADC_H_ */
