/**
 * \file    eyebot.h
 * \brief   Header file for the EyeBot functions
 * \author  Marcus Pham & Remi Keat
 */

#ifndef EYEBOT_H_
#define EYEBOT_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "camera.h"
#include "servos_and_motors.h"
#include "psd_sensors.h"
#include "timer.h"
#include "multitasking.h"
#include "key.h"
#include "system.h"
#include "adc.h"
#include "imageProc.h"
#include "irtv.h"
#include "vomega.h"
#include "lcd.h"
#include "serial.h"
#include "inout.h"
#include "audio.h"
#include "hdt.h"
#include "types.h"

#ifdef __cplusplus
}
#endif



#endif /* EYEBOT_H_ */
