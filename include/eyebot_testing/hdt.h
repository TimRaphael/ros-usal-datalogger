/**
 * \file    hdt.h
 * \brief   Header file for the HDT functions
 * \author  Remi KEAT & Marcus Pham
 */

#ifndef HDT_H_
#define HDT_H_



#ifdef __cplusplus
extern "C" {
#endif
#include "types.h"
#include <stdio.h> /* FILE */
#include <stdlib.h> /* malloc() */
#include <string.h> /* strstr() */
#ifdef __cplusplus
}
#endif

 


#define HDT_IDX_TABLE	0
#define HDT_IDX_PSD		1
#define HDT_IDX_SERVO	2
#define HDT_IDX_MOTOR	3
#define HDT_IDX_ENCODER	4
#define HDT_IDX_DRIVE	5
#define HDT_IDX_COMPASS	6
#define HDT_IDX_IRTV	7
#define HDT_IDX_CAM		8
#define HDT_IDX_ADC		9
#define HDT_IDX_COM		10
#define HDT_MAX_COUNT	11

#define DIFFERENTIAL_DRIVE 0
#define ACKERMAN_DRIVE 1
#define ACKERMANN_DRIVE 1
#define SYNCHRO_DRIVE 2
#define TRICYCLE_DRIVE 3
#define OMNI_DRIVE 4

#define HDT_DIFF_STR "DIFFERENTIAL"
#define HDT_ACKM_STR "ACKERMANN"
#define HDT_OMNI_STR "OMNI"

int HDTValidate(char *filename);
int HDTListEntry(char *filename, HDT_ENTRY *deventry, int count);
int HDTFindEntry(void *hdtfile, char *devname, HDT_ENTRY *deventry);
int HDTFindTable(void *hdtfile, char *tabname, HDT_TABLE *tabentry);
HDT_TABLE* HDTLoadTable(char *filename, HDT_DEVICE *pdevices);
int HDTClearTable(HDT_TABLE *ptables);
HDT_CAM* HDTLoadCAM(char *filename, char *devname);
int HDTClearCAM(HDT_CAM *pdevs);
HDT_MOTOR* HDTLoadMOTOR(char *filename, char *devname);
int HDTClearMOTOR(HDT_MOTOR *pdevs);
HDT_ENCODER* HDTLoadENCODER(char *filename, char *devname);
int HDTClearENCODER(HDT_ENCODER *pdevs);
int HDTLinkENC2MOT(HDT_ENCODER *pencoders, HDT_MOTOR *pmotors);
HDT_PSD* HDTLoadPSD(char *filename, char *devname);
int HDTClearPSD(HDT_PSD *pdevs);
HDT_SERVO* HDTLoadSERVO(char *filename, char *devname);
int HDTClearSERVO(HDT_SERVO *pdevs);
HDT_DRIVE* HDTLoadDRIVE(char *filename, char *devname);
int HDTClearDRIVE(HDT_DRIVE *pdevs);
int HDTLinkDRV2ENC(HDT_DRIVE *pdrives, HDT_ENCODER *pencoders);
HDT_IRTV* HDTLoadIRTV(char *filename, char *devname);
int HDTClearIRTV(HDT_IRTV *pdevs);
HDT_ADC* HDTLoadADC(char *filename, char *devname);
int HDTClearADC(HDT_ADC *pdevs);
HDT_COM* HDTLoadCOM(char *filename, char *devname);
int HDTClearCOM(HDT_COM *pdevs);

#endif /* HDT_H_ */
