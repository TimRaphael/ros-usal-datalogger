/**
 * \file		misc.h
 * \brief   Header file for misc functions
 * \author  Remi KEAT
 */
#ifndef MISC_H_
#define MISC_H_

#ifdef __cplusplus
extern "C" {
#endif
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#ifdef __cplusplus
}
#endif



void strcpy_n(char* __dest, const char* __src, size_t __n);
int max(int a, int b);
void trimline(char *src);

#endif /* MISC_H_ */
