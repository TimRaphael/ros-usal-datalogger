/**
 * \file    multitasking.h
 * \brief   Header file for multitasking functions
 * \author  Marcus Pham
 */


#ifndef MULTITASKING_H_
#define MULTITASKING_H_




#ifdef __cplusplus
extern "C" {
#endif
#include "types.h"
#include <pthread.h>
#include <semaphore.h>
#ifdef __cplusplus
}
#endif


/**
 * \brief Structure defining a TASK item
 */
typedef struct
{
    pthread_t thread;
    int identifier;
} TASK;

typedef sem_t SEMA;

int  MTInit();                            // Start multi-tasking
int  MTSleep(int n);                       // Sleep for n/100 seconds
TASK MTSpawn(void* (*) (void*), int);   //Create and initialize thread with given ID
int  MTGetUID(TASK);                      // Read ID of current thread
int  MTKill(TASK);                        // Delete thread (0 for self)
int  MTExit(void);                        // Terminate current thread


int SEMAInit(SEMA *sem, int val);    // Create and initialiye semaphore with VAL
int  SEMALock(SEMA* sem);                  // Lock semaphore
int  SEMAUnlock(SEMA* sem);                    // Unlock semaphore

#endif /*MULTITASKING_H_*/