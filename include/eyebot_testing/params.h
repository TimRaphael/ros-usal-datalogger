/**
 * \file		params.h
 * \author  Remi KEAT
 * \brief   Defines main parameters
 */

#ifdef __cplusplus
extern "C" {
#endif
#include <math.h>
#ifdef __cplusplus
}
#endif


//#include "lcd.h"
//#include "types.h"

#ifndef PARAMS_H_
#define PARAMS_H_

#define VEHICLE 1
#define PLATFORM 2
#define WALKER 3

#define DEBUG 1
//#define MIN(a,b) (((a)<(b))?(a):(b))
//#define MAX(a,b) (((a)>(b))?(a):(b))



#define NUMBER_TRY 10
#define HDT_MAX_NAMECHAR 80
#define LCD_MENU_STRLENGTH 32 /* for storage declaration */
#define LCD_LIST_STRLENGTH 64 /* for storage declaration */
#define MENU_HEIGHT 38
#define KEYTM_MAX_REGIONS 32
#define VERSION "1.0"
#define MACHINE_SPEED 700000000
#define MACHINE_TYPE VEHICLE
#define MACHINE_NAME "EyeBot"
#define ID 1
#define LIBM6OS_VERSION "1.0"

#define LCD_HEIGHT 320 //240 for RPi1 320 for Rpi2
#define LCD_WIDTH 480 //320 for Rpi1 480 for RPi2

#define LCD_MAX_LIST_ITEM 10
//#define LCD_MAX_LIST_ITEM MAX(1,(gLCDHandle->height/(gLCDHandle->fontHeight*2))-3)
#define HDT_FILE "/home/pi/eyebot/bin/hdt.txt"

#define HDT_MAX_PATHCHAR 256
#define HDT_MAX_FILECHAR 40
#define HDT_MAX_READBUFF 128

#define IOBOARD 1

#endif /* PARAMS_H_ */
