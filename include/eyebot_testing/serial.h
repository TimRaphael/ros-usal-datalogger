/**
 * \file   serial.h
 * \brief  Defines all the functions to connect to a serial USB/serial connection
 * \author Marcus Pham
 */

#ifndef SERIAL_H_
#define	SERIAL_H_




#ifdef __cplusplus
extern "C" {
#endif
#include "types.h"
#include "system.h"
#include <stdio.h>     // Standard input/output definitions
#include <string.h>    // String function definitions
#include <unistd.h>   // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <errno.h>     // Error number definitions
#include <termios.h>  // POSIX terminal control definitions
#include <time.h>
#include <stdbool.h> //For boolean usage
#ifdef __cplusplus
}
#endif



#define TIMEOUT 100
#define DEFAULT_BAUD 115200

char USBPATH1[HDT_MAX_PATHCHAR];
char USBPATH2[HDT_MAX_PATHCHAR];
char USBPATH3[HDT_MAX_PATHCHAR];
char USBPATH4[HDT_MAX_PATHCHAR];

int SERPathInit;
int SERType;
int SERBaud[5];
int SERHandshake[5];

int  SERInit(int interface, int baud,int handshake); // Init communication (see parameters below)
int  SERSendChar(int interface, char ch);            // Send single character
int  SERSend(int interface, char *buf);              // Send string (Null terminated)
int  SERReceive(int interface, char *ch);            // Receive String (Null terminated)
char SERReceiveChar(int interface);                     // Receive single character
bool SERCheck(int interface);                       // Non-blocking check if character is waiting
int  SERFlush(int interface);                      // Flush interface buffers
int  SERClose(int interface);                       // Close Interface
int  SERGetPaths();

int fd;                                             //the default port


#endif	/* SERIAL_H */

