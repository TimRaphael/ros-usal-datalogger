/**
 * \file	system.h
 * \brief   Header file for system functions
 * \author  Remi KEAT & Marcus Pham
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_


#ifdef __cplusplus
extern "C" {
#endif
#include "types.h"
#include "string.h"
#include <sys/sysinfo.h>
#include <stdio.h>
#ifdef __cplusplus
}
#endif




void strcpy_n(char* __dest, const char* __src, size_t __n);
char* OSExecute(char* command);

char *OSVersion(void);
int OSMachineSpeed(void);
int OSMachineType(void);
char* OSMachineName(void);
unsigned char OSMachineID(void);

int OSError(char *msg, int number, bool deadend);
int OSInfoCPU (INFO_CPU* infoCPU);
int OSInfoMem (INFO_MEM* infoMem);
int OSInfoProc (INFO_PROC* infoProc);
int OSInfoMisc (INFO_MISC* infoMisc);

#endif /* SYSTEM_H_ */
