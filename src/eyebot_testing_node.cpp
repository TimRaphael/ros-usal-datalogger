#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>
#include <string>
#include "eyebot_testing/motor_speed.h"

#ifdef __cplusplus
extern "C" {
#endif
#include "eyebot_testing/servos_and_motors.h"
#include "eyebot_testing/psd_sensors.h"
#ifdef __cplusplus
}
#endif

int reqMotorSpeed = 0;

bool motor_speed(eyebot_testing::motor_speed::Request &req, eyebot_testing::motor_speed::Response &res)
{
		reqMotorSpeed = req.speed;
		res.status = 0;

		return true;
}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "eyebot_testing_node");
	ros::NodeHandle n;

	ros::ServiceServer speed_service = n.advertiseService("motor_speed", motor_speed);

	ros::Publisher motor_status_pub = n.advertise<std_msgs::String>("motor_status", 1000);

	ros::Rate loop_rate(10);

	int status;

	while(ros::ok())
	{

		//status = MOTORDriveRaw(1,reqMotorSpeed);
		status = PSDGetRaw(0);
		std_msgs::String msg;

		std::stringstream ss;
		ss << "PSD Status: " << status;
		msg.data = ss.str();
	
		ROS_INFO("%s", msg.data.c_str());

		motor_status_pub.publish(msg);

		ros::spinOnce();

		loop_rate.sleep();
	}


	//MOTORDriveRaw(1,0);
	return 0;
}
