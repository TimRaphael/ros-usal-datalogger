#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/Imu.h"

#include <iostream>
#include <fstream>

std::ofstream dataFile;

std::string currentMotorSpeed;

void cmdMessageCallback(const std_msgs::String::ConstPtr& msg)
{
	currentMotorSpeed = msg->data.c_str();
}

void imuMessageCallback(const sensor_msgs::Imu::ConstPtr& msg)
{
	// Output format: {sequence, seconds, nano seconds} {acceleration x, y,z}
	//std::ostringstream velocity_ss; 
	std::ostringstream accel_ss; 
	std::ostringstream vel_ss;
	std::ostringstream orien_ss;
	std::ostringstream timestamp;


	//velocity_ss << msg->angular_velocity.x << "," << msg->angular_velocity.y << "," << msg->angular_velocity.z << ",";
	timestamp << msg->header.seq << "," << msg->header.stamp.sec << "," << msg->header.stamp.nsec;
	accel_ss << msg->linear_acceleration.x << "," << msg->linear_acceleration.y << "," << msg->linear_acceleration.z;
	vel_ss << msg->angular_velocity.x << "," << msg->angular_velocity.y << "," << msg->angular_velocity.z;
	orien_ss << msg->orientation.x << "," << msg->orientation.y << "," << msg->orientation.z << "," << msg->orientation.w;

	dataFile << timestamp.str() << "," << currentMotorSpeed << "," << accel_ss.str() << "," << vel_ss.str() << "," << orien_ss.str() << '\n';
	//dataFile << timestamp.str() << "," << currentMotorSpeed << "," << accel_ss.str() << "," << vel_ss.str() << '\n';
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "usal_datalogger");

	ros::NodeHandle n;

	dataFile.open ("/tmp/output_data.txt");

	currentMotorSpeed = "0";

	ros::Subscriber usal_dir_cmd_sub = n.subscribe("usal_dir_cmd", 1000, cmdMessageCallback);
	ros::Subscriber imu_sub = n.subscribe("/imu/data", 1000, imuMessageCallback);

	ros::spin();

	dataFile.close();


	return 0;

}
